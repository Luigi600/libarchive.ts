#define LIBARCHIVE_STATIC
#include <archive.h>
#include <archive_entry.h>
#include <emscripten.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

EMSCRIPTEN_KEEPALIVE
struct archive *create_archive(const int format) {
  struct archive *archive = archive_write_new();

  if(archive_write_set_format(archive, format) != ARCHIVE_OK)
    return NULL;

  return archive;
}

EMSCRIPTEN_KEEPALIVE
const int write_entry(
  struct archive *archive,
  struct archive_entry *entry,
  const void *buff,
  size_t buf_size
) {
  int result;
  archive_entry_set_size(entry, buf_size);

  result = archive_write_header(archive, entry);
  if(result != ARCHIVE_OK)
    return result;

  result = archive_write_data(archive, buff, buf_size);
  if(result <= 0) // written length
    return ARCHIVE_FATAL;

  archive_entry_free(entry);
  return ARCHIVE_OK;
}

EMSCRIPTEN_KEEPALIVE
void close_archive(void *archive) {
  archive_write_close(archive);
  archive_write_free(archive);
}
