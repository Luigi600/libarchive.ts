#!/bin/bash

function build {
  emcc ./libarchive_read.c ./libarchive_write.c \
  	-I /usr/local/include /usr/local/lib/libarchive.a \
  	/usr/local/lib/libz.a /usr/local/lib/libbz2.a /usr/local/lib/liblzma.a /usr/local/lib/libssl.a /usr/local/lib/libcrypto.a \
  	-o ./build/$2/libarchive.js \
  	-s MODULARIZE=1 -s EXPORT_NAME=libarchive -s WASM=1 -O3 -s ALLOW_MEMORY_GROWTH=1 \
  	-s EXPORTED_RUNTIME_METHODS='["cwrap", "FS"]' -s EXPORTED_FUNCTIONS=@./exported_functions.json \
  	-s ENVIRONMENT=$1 \
  	-s ERROR_ON_UNDEFINED_SYMBOLS=1
}

build "web", "browser"
build "node", "node"
