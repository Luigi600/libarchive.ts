import libarchive from './libarchive';
import { wrapper } from './wrapper';

export type LibarchiveWasm = ReturnType<typeof wrapper>;

export async function libarchiveWasm(
  ...args: Parameters<typeof libarchive>
): Promise<LibarchiveWasm> {
  const mod = await libarchive(...args);
  return wrapper(mod);
}
