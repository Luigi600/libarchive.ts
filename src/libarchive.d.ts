import type { LibarchiveModule } from './LibarchiveModule';

declare function libarchive(options?: {
  locateFile?: (path: string, prefix: string) => string;
}): Promise<LibarchiveModule>;

// eslint-disable-next-line import/no-default-export
export default libarchive;
