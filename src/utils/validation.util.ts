export function nonZero(r: number): boolean {
  return r !== 0;
}

export function isZero(r: number): boolean {
  return r === 0;
}
