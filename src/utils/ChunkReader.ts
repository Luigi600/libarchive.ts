/* eslint-disable no-underscore-dangle */

const DEFAULT_CHUNK_SIZE = 1024 * 512; // in bytes

export class ChunkReader {
  protected readonly _file: Blob;

  protected readonly _readLength: number;

  protected readonly _reader = new FileReader();

  protected readonly _chunkSize: number;

  public constructor(file: Blob) {
    this._file = file;
    this._chunkSize = DEFAULT_CHUNK_SIZE;
    this._readLength = file.size;

    if (this._readLength < 0 || this._readLength > file.size)
      throw new Error('Invalid read length!');
  }

  public async readChunkBytes(offset: number): Promise<Uint8Array> {
    const chunk = await this.readChunk(offset);
    if (!chunk) throw new Error('Invalid chunk! Chunk was empty.');

    return new Uint8Array(chunk);
  }

  protected readChunk(offset: number): Promise<ArrayBuffer | null> {
    return new Promise((resolve, reject) => {
      this._reader.onloadend = (): void => {
        resolve(this._reader.result as ArrayBuffer | null);
      };
      this._reader.onerror = (e): void => {
        reject(e);
      };

      const chunk = this._file.slice(offset, Math.min(offset + this._chunkSize, this._readLength));
      this._reader.readAsArrayBuffer(chunk);
    });
  }
}
