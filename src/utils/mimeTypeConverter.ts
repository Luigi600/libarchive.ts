import { ArchiveFormat } from '../ArchiveFormat';

const CONVERTER: Record<number, string> = {
  [ArchiveFormat.TAR]: 'application/x-tar',
  [ArchiveFormat.RAR]: 'application/vnd.rar',
  [ArchiveFormat.RAR_5]: 'application/vnd.rar',
  [ArchiveFormat.ZIP]: 'application/zip',
  [ArchiveFormat.SEVEN_ZIP]: 'application/x-7z-compressed',
};

export function convertArchiveFormatToMimeType(format: ArchiveFormat): string {
  const converted = CONVERTER[format];
  if (converted) return converted;

  return 'application/octet-stream';
}
