/**
 * Specifies how many milliseconds make up a second.
 *
 * This constant allows the conversion of the C `time_t` type into JavaScript-friendly `Date` time.
 */
const ONE_SEC_IN_MS = 1000;

export function convertSecondsToMilliseconds(secs: number): number {
  return secs * ONE_SEC_IN_MS; // convert secs to ms
}

export function convertMillisecondsToSeconds(ms: number): number {
  return Math.trunc(ms / ONE_SEC_IN_MS);
}
