export interface EntryOptions {
  permission?: number;
  creationTime?: Date;
  modificationTime?: Date;
}
