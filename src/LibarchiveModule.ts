// cf. https://github.com/DefinitelyTyped/DefinitelyTyped/blob/master/types/emscripten/index.d.ts

interface JSTypes {
  number: number;
  string: string;
  // eslint-disable-next-line @typescript-eslint/no-invalid-void-type
  null: void;
}

type JSTypeStrings = keyof JSTypes;

type ToJSType<S> = S extends null ? null : S extends JSTypeStrings ? JSTypes[S] : unknown;

type ToJSTypeArray<SA> = SA extends readonly [infer S, ...infer R]
  ? readonly [ToJSType<S>, ...ToJSTypeArray<R>]
  : readonly [];

export interface LibarchiveModule {
  cwrap: <
    RT extends ToJSType<RS>,
    TA = undefined,
    RS extends JSTypeStrings = JSTypeStrings,
    SA extends readonly JSTypeStrings[] = readonly JSTypeStrings[],
  >(
    ident: string,
    returnType: RS,
    argTypes: SA,
  ) => (...args: TA extends unknown[] ? TA : ToJSTypeArray<SA>) => RT;

  FS: {
    readFile: (path: string) => Uint8Array;
    unlink: (path: string) => void;
  };

  HEAP8: Uint8Array;

  _malloc: (size: number) => number;
  _free: (ptr: number) => void;
}
