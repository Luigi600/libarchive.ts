/* eslint-disable no-underscore-dangle */
import { ArchiveReaderEntry } from './ArchiveReaderEntry';
import type { FileType } from './FileType';
import type { LibarchiveWasm } from './libarchiveWasm';
import { convertSecondsToMilliseconds } from './utils/convert.util';

export class ArchiveReader {
  private _libarchive: LibarchiveWasm | null;

  private _archive: number | null;

  private _pointer: number | null;

  constructor(libarchive: LibarchiveWasm, data: Uint8Array, passphrase?: string) {
    const ptr = libarchive.module._malloc(data.length);
    libarchive.module.HEAP8.set(data, ptr);

    this._libarchive = libarchive;
    this._archive = libarchive.read_new_memory(ptr, data.length, passphrase);
    this._pointer = ptr;
  }

  free(): void {
    if (!this._libarchive || !this._archive || !this._pointer)
      throw new Error('Archive is already closed.');

    this._libarchive.read_free(this._archive);
    this._libarchive.module._free(this._pointer);

    this._libarchive = null;
    this._archive = null;
    this._pointer = null;
  }

  hasEncryptedData(): boolean | null {
    if (!this._libarchive || !this._archive) throw new Error('Archive is already closed.');

    const code = this._libarchive.read_has_encrypted_entries(this._archive);
    return code < 0 ? null : !!code;
  }

  readData(size: number): Uint8Array {
    if (!this._libarchive || !this._archive) throw new Error('Archive is already closed.');

    const eptr = this._libarchive.module._malloc(size);
    const esize = this._libarchive.read_data(this._archive, eptr, size);
    const data = this._libarchive.module.HEAP8.slice(eptr, eptr + esize);
    this._libarchive.module._free(eptr);

    return data;
  }

  skipData(): void {
    if (!this._libarchive || !this._archive) throw new Error('Archive is already closed.');

    this._libarchive.read_data_skip(this._archive);
  }

  nextEntryPointer(): number {
    if (!this._libarchive || !this._archive) throw new Error('Archive is already closed.');

    return this._libarchive.read_next_entry(this._archive);
  }

  getEntryFiletype(ptr: number): FileType {
    if (!this._libarchive || !this._archive) throw new Error('Archive is already closed.');

    return this._libarchive.entry_filetype(ptr);
  }

  getEntryPathname(ptr: number): string {
    if (!this._libarchive || !this._archive) throw new Error('Archive is already closed.');

    return this._libarchive.entry_pathname(ptr);
  }

  getEntrySize(ptr: number): number {
    if (!this._libarchive || !this._archive) throw new Error('Archive is already closed.');

    return this._libarchive.entry_size(ptr);
  }

  getCreationTime(ptr: number): Date {
    if (!this._libarchive) throw new Error('Archive is already closed.');

    return new Date(convertSecondsToMilliseconds(this._libarchive.entry_ctime(ptr)));
  }

  getModificationTime(ptr: number): Date {
    if (!this._libarchive) throw new Error('Archive is already closed.');

    return new Date(convertSecondsToMilliseconds(this._libarchive.entry_mtime(ptr)));
  }

  isEntryEncrypted(ptr: number): boolean {
    if (!this._libarchive) throw new Error('Archive is already closed.');

    return !!this._libarchive.entry_is_encrypted(ptr);
  }

  nextEntry(): ArchiveReaderEntry | null {
    const entryPtr = this.nextEntryPointer();
    if (entryPtr === 0) return null;
    return new ArchiveReaderEntry(this, entryPtr);
  }

  forEach(fn: (entry: ArchiveReaderEntry) => unknown): void {
    for (;;) {
      const entry = this.nextEntry();
      if (!entry) break;
      fn(entry);
      entry.skipData();
    }
  }

  *entries(): Generator<ArchiveReaderEntry, void, undefined> {
    let entry: ArchiveReaderEntry | null = null;
    // eslint-disable-next-line no-cond-assign
    while ((entry = this.nextEntry())) {
      yield entry;
      entry.skipData();
    }
  }
}
