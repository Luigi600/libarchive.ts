/**
 * Specifies how many milliseconds make up a second.
 *
 * This constant allows the conversion of the C `time_t` type into JavaScript-friendly `Date` time.
 */
export const ONE_SEC_IN_MS = 1000;
