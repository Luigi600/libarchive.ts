/* eslint-disable no-underscore-dangle */
import { ArchiveFormat } from './ArchiveFormat';
import type { EntryOptions } from './EntryOptions';
import { FileType } from './FileType';
import type { LibarchiveWasm } from './libarchiveWasm';
import { ChunkReader } from './utils/ChunkReader';
import { convertMillisecondsToSeconds } from './utils/convert.util';
import { convertArchiveFormatToMimeType } from './utils/mimeTypeConverter';

export class ArchiveWriter {
  private readonly _libarchive: LibarchiveWasm;

  private _archive: number | null;

  private _nameWasWritten = false;

  private readonly _name: string;

  private readonly _archiveFormat: ArchiveFormat;

  constructor(libarchive: LibarchiveWasm, name: string, format: ArchiveFormat = ArchiveFormat.ZIP) {
    this._libarchive = libarchive;
    this._archive = libarchive.create_archive(format);
    this._name = name;
    this._archiveFormat = format;
  }

  setCompression(compression: string): void {
    if (!this._archive) throw new Error('Archive is already closed.');

    if (this._nameWasWritten)
      throw new Error('Compression must be set before writing files to the archive!');

    this._libarchive.archive_set_options(this._archive, `compression=${compression}`);
  }

  setCompressionLevel(level: number): void {
    if (!this._archive) throw new Error('Archive is already closed.');

    if (this._nameWasWritten)
      throw new Error('Compression level must be set before writing files to the archive!');

    this._libarchive.archive_set_options(this._archive, `compression-level=${level}`);
  }

  addEntryBytes(name: string, data: Uint8Array, options: EntryOptions): void {
    if (!this._archive) throw new Error('Archive is already closed.');

    this.writeArchiveFilename();

    const entry = this.createEntry(name, options);

    const ptr = this._libarchive.module._malloc(data.length);
    this._libarchive.module.HEAP8.set(data, ptr);
    this._libarchive.write_entry(this._archive, entry, ptr, data.length);
    this._libarchive.module._free(ptr);
  }

  async addEntryFile(file: File, options: EntryOptions): Promise<void> {
    return this.addEntryBlob(file.name, file, options);
  }

  async addEntryBlob(name: string, data: Blob, options: EntryOptions): Promise<void> {
    if (!this._archive) throw new Error('Archive is already closed.');

    this.writeArchiveFilename();

    const entry = this.createEntry(name, options);

    const ptr = this._libarchive.module._malloc(data.size);

    let seek = 0;
    const reader = new ChunkReader(data);
    while (seek < data.size) {
      // eslint-disable-next-line no-await-in-loop
      const chunk = await reader.readChunkBytes(seek);

      this._libarchive.module.HEAP8.set(chunk, ptr + seek);

      seek += chunk.length;
    }

    this._libarchive.write_entry(this._archive, entry, ptr, data.size);
    this._libarchive.module._free(ptr);
  }

  close(): void {
    if (!this._archive) throw new Error('Archive is already closed.');

    this._libarchive.archive_close(this._archive);
    this._archive = null;
  }

  getFile(): File {
    return new File([this.getBlob()], this._name);
  }

  getBlob(): Blob {
    const file = this._libarchive.module.FS.readFile(this._name);
    this._libarchive.module.FS.unlink(this._name);
    return new Blob([file], { type: convertArchiveFormatToMimeType(this._archiveFormat) });
  }

  private createEntry(name: string, options: EntryOptions): number {
    const entry = this._libarchive.archive_entry_new();

    this._libarchive.archive_entry_set_pathname(entry, name);
    this._libarchive.archive_entry_set_filetype(entry, FileType.FILE);

    if (options.permission !== undefined)
      this._libarchive.archive_entry_set_perm(entry, options.permission);

    if (options.creationTime !== undefined)
      this._libarchive.archive_entry_set_ctime(
        entry,
        convertMillisecondsToSeconds(options.creationTime.getTime()),
        0,
      );

    if (options.modificationTime !== undefined)
      this._libarchive.archive_entry_set_mtime(
        entry,
        convertMillisecondsToSeconds(options.modificationTime.getTime()),
        0,
      );

    return entry;
  }

  private writeArchiveFilename(): void {
    if (!this._archive) throw new Error('Archive is already closed.');

    if (this._nameWasWritten) return;

    this._libarchive.archive_write_filename(this._archive, this._name);
    this._nameWasWritten = true;
  }
}
