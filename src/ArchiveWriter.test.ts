import { ArchiveWriter } from './ArchiveWriter';
import { FileType } from './FileType';
import { libarchiveWasm } from './libarchiveWasm';

describe('ArchiveWriter', () => {
  test('should create an archive with the wrapper class with much less code', async () => {
    const mod = await libarchiveWasm();
    const writer = new ArchiveWriter(mod, 'memory_write.zip');
    writer.setCompression('deflate');
    writer.setCompressionLevel(9);

    const buffer = new TextEncoder().encode('Hello World! Hello World! Hello World! Hello World!');
    writer.addEntryBytes('test/file.txt', buffer, {
      permission: 0o007,
      modificationTime: new Date('2016-05-03 14:02:21:51'),
      creationTime: new Date('2016-05-03 14:02:21:51'),
    });

    writer.close();

    const file = writer.getBlob();
    expect(file.size).toBeGreaterThan(50);
    expect(file.type).toEqual('application/zip');
  });

  test('should create an archive with the wrapper class with a simple browser Blob object', async () => {
    const mod = await libarchiveWasm();
    const writer = new ArchiveWriter(mod, 'memory_write.zip');

    const fileReaderSpy = jest.fn(async function test(this: FileReader, blob: Blob) {
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-expect-error
      this.result = await blob.arrayBuffer();
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-expect-error
      this.onloadend({});
    });
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-expect-error
    global.FileReader = jest.fn(() => ({
      result: null,
      readAsArrayBuffer: fileReaderSpy,
    }));

    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-expect-error
    global.File = jest.fn((_, fileName) => ({
      name: fileName,
      type: fileName.split('.')[1],
    }));

    writer.setCompression('deflate');
    writer.setCompressionLevel(9);

    await writer.addEntryBlob('README.md', new Blob(['Hello world!']), {
      permission: 0o007,
      modificationTime: new Date('2016-05-03 14:02:21:51'),
      creationTime: new Date('2016-05-03 14:02:21:51'),
    });

    expect(fileReaderSpy.mock.calls.length).toEqual(1);

    writer.close();

    const file = writer.getBlob();
    expect(file.size).toBeGreaterThan(50);
    expect(file.type).toEqual('application/zip');
  });

  /**
   * https://manpages.debian.org/unstable/libarchive-dev/archive_write_set_options.3.en.html
   * https://github.com/libarchive/libarchive/issues/476#issuecomment-91882657
   */
  test('should create an archive using the raw C functions', async () => {
    const mod = await libarchiveWasm();
    const archive = mod.create_archive(0xe0000);
    const outputFile = 'test.rar';

    mod.archive_set_options(archive, 'compression=deflate,compression-level=0');
    mod.archive_write_filename(archive, outputFile);

    const buffer = new TextEncoder().encode('Hello World! Hello World! Hello World! Hello World!');
    // eslint-disable-next-line no-underscore-dangle
    const ptr = mod.module._malloc(buffer.length);
    mod.module.HEAP8.set(buffer, ptr);
    // mod.archive_write_entry(archive, 'test.txt', 0o100000, 0o777, ptr, buffer.length);
    const entry = mod.archive_entry_new();
    mod.archive_entry_set_pathname(entry, 'test.txt');
    mod.archive_entry_set_filetype(entry, FileType.FILE);
    mod.archive_entry_set_perm(entry, 0o700);
    const ctime = new Date('2020-01-01');
    const ctimeSecs = ctime.getTime() / 1000;
    mod.archive_entry_set_ctime(entry, ctimeSecs, 0);
    mod.archive_entry_set_mtime(entry, ctimeSecs, 0);
    mod.write_entry(archive, entry, ptr, buffer.length);
    mod.archive_close(archive);

    const file = mod.module.FS.readFile(outputFile);
    mod.module.FS.unlink(outputFile);

    expect(file.length).toBeGreaterThan(50);
  });
});
