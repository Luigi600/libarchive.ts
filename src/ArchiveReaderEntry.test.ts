import { readFile } from 'fs/promises';

import { ArchiveReader } from './ArchiveReader';
import { libarchiveWasm } from './libarchiveWasm';

describe('ArchiveReaderEntry', () => {
  test('verification that time of creation or modification is plausible', async () => {
    const data = await readFile('./archives/deflate.zip');
    const mod = await libarchiveWasm();
    const a = new ArchiveReader(mod, new Uint8Array(data));
    a.forEach((entry) => {
      const mtime = entry.getModificationTime().getTime();
      const ctime = entry.getCreationTime().getTime();

      expect(mtime > ctime).toBeTruthy();
      expect(mtime).toBeGreaterThan(new Date('2020-01-01').getTime());
    });
    a.free();
  });

  test('test Chinese chars in path with locale', async () => {
    const data = await readFile('./archives/utf8.zip');
    const mod = await libarchiveWasm();
    mod.set_locale('en_US.UTF-8');
    const a = new ArchiveReader(mod, new Uint8Array(data));
    const entry = a.nextEntry();
    expect(entry).toBeDefined();
    expect('自述文件.txt').toEqual(entry?.getPathname());
  });
});
