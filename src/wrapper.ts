import type { LibarchiveModule } from './LibarchiveModule';
import { isZero, nonZero } from './utils/validation.util';

// eslint-disable-next-line @typescript-eslint/explicit-function-return-type
export function wrapper(module: LibarchiveModule) {
  /* eslint-disable @typescript-eslint/no-explicit-any */
  const checkReturnValue = <R, F extends (...args: any[]) => R>(
    fn: F,
    test: (r: R) => boolean,
  ): F =>
    function f(this: { error_string: (_: any) => string }, ...args: any[]) {
      // eslint-disable-next-line @typescript-eslint/no-unsafe-argument
      const r = fn(...args);
      if (test(r)) throw new Error(this.error_string(args[0]));
      return r;
    } as unknown as F;
  /* eslint-enable @typescript-eslint/no-explicit-any */

  return {
    module,
    // generic
    set_locale: module.cwrap('set_locale', 'null', ['string'] as const),
    version_number: module.cwrap('archive_version_number', 'number', []),
    version_string: module.cwrap('archive_version_string', 'string', []),
    version_details: module.cwrap('archive_version_details', 'string', []),
    // read
    read_new_memory: checkReturnValue(
      module.cwrap<number, [number, number, string?]>('read_archive_memory', 'number', [
        'number',
        'number',
        'string',
      ] as const),
      isZero,
    ),
    read_new: module.cwrap('archive_read_new', 'number', []),
    read_support_filter_all: checkReturnValue(
      module.cwrap('archive_read_support_filter_all', 'number', ['number'] as const),
      nonZero,
    ),
    read_support_format_all: checkReturnValue(
      module.cwrap('archive_read_support_format_all', 'number', ['number'] as const),
      nonZero,
    ),
    read_open_memory: checkReturnValue(
      module.cwrap('archive_read_open_memory', 'number', ['number', 'number', 'number'] as const),
      nonZero,
    ),
    read_next_entry: module.cwrap('read_next_entry', 'number', ['number'] as const),
    read_has_encrypted_entries: module.cwrap('archive_read_has_encrypted_entries', 'number', [
      'number',
    ] as const),
    read_data: checkReturnValue(
      module.cwrap('archive_read_data', 'number', ['number', 'number', 'number'] as const),
      (r: number) => r < 0,
    ),
    read_data_skip: checkReturnValue(
      module.cwrap('archive_read_data_skip', 'number', ['number'] as const),
      nonZero,
    ),
    read_add_passphrase: checkReturnValue(
      module.cwrap('archive_read_add_passphrase', 'number', ['number', 'string'] as const),
      nonZero,
    ),
    read_free: checkReturnValue(
      module.cwrap('archive_read_free', 'number', ['number'] as const),
      nonZero,
    ),
    error_string: module.cwrap('archive_error_string', 'string', ['number'] as const),
    entry_filetype: module.cwrap('archive_entry_filetype', 'number', ['number'] as const),
    entry_pathname: module.cwrap('archive_entry_pathname_utf8', 'string', ['number'] as const),
    entry_size: module.cwrap('archive_entry_size', 'number', ['number'] as const),
    entry_ctime: module.cwrap('archive_entry_ctime', 'number', ['number'] as const),
    entry_mtime: module.cwrap('archive_entry_mtime', 'number', ['number'] as const),
    entry_is_encrypted: module.cwrap('archive_entry_is_encrypted', 'number', ['number'] as const),
    // write
    archive_close: module.cwrap('close_archive', 'null', ['number'] as const),
    archive_entry_new: checkReturnValue(module.cwrap('archive_entry_new', 'number', []), isZero),
    archive_entry_set_ctime: module.cwrap('archive_entry_set_ctime', 'null', [
      'number',
      'number',
      'number',
    ] as const),
    archive_entry_set_filetype: module.cwrap('archive_entry_set_filetype', 'null', [
      'number',
      'number',
    ] as const),
    archive_entry_set_mtime: module.cwrap('archive_entry_set_mtime', 'number', [
      'number',
      'number',
      'number',
    ] as const),
    archive_entry_set_pathname: module.cwrap('archive_entry_set_pathname', 'null', [
      'number',
      'string',
    ] as const),
    archive_entry_set_perm: module.cwrap('archive_entry_set_perm', 'null', [
      'number',
      'number',
    ] as const),
    archive_write_format: module.cwrap('archive_write_set_format', 'null', [
      'number',
      'number',
    ] as const),
    archive_set_options: checkReturnValue(
      module.cwrap('archive_write_set_options', 'number', ['number', 'string'] as const),
      nonZero,
    ),
    archive_write_filename: checkReturnValue(
      module.cwrap('archive_write_open_filename', 'number', ['number', 'string'] as const),
      nonZero,
    ),
    create_archive: checkReturnValue(
      module.cwrap('create_archive', 'number', ['number'] as const),
      isZero,
    ),
    write_entry: checkReturnValue(
      module.cwrap('write_entry', 'number', ['number', 'number', 'number', 'number'] as const),
      nonZero,
    ),
  };
}
