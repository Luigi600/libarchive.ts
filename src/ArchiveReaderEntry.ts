/* eslint-disable no-underscore-dangle */
import type { ArchiveReader } from './ArchiveReader';
import type { FileType } from './FileType';

export class ArchiveReaderEntry {
  private readonly _reader: ArchiveReader;

  private readonly _pointer: number;

  private _readCalled: boolean;

  constructor(reader: ArchiveReader, ptr: number) {
    this._reader = reader;
    this._pointer = ptr;
    this._readCalled = false;
  }

  readData(): Uint8Array | undefined {
    if (this._readCalled) throw new Error('It has already been called.');

    const size = this.getSize();
    if (!size) {
      this.skipData();
      return undefined;
    }

    this._readCalled = true;
    return this._reader.readData(size);
  }

  skipData(): void {
    if (this._readCalled) return;
    this._readCalled = true;
    this._reader.skipData();
  }

  getFiletype(): FileType {
    return this._reader.getEntryFiletype(this._pointer);
  }

  getPathname(): string {
    return this._reader.getEntryPathname(this._pointer);
  }

  getSize(): number {
    return this._reader.getEntrySize(this._pointer);
  }

  getCreationTime(): Date {
    return this._reader.getCreationTime(this._pointer);
  }

  getModificationTime(): Date {
    return this._reader.getModificationTime(this._pointer);
  }

  isEncrypted(): boolean {
    return this._reader.isEntryEncrypted(this._pointer);
  }
}
